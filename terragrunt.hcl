remote_state {
  backend = "gcs"

  generate = {
    path      = "backend.tf"
    if_exists = "overwrite_terragrunt"
  }
  
  config = {
    project  = "ari-admin-1"
    location = "us"
    bucket   = "ari-admin-backend-1"
    prefix   = "${path_relative_to_include()}/terraform.tfstate"
  }
}