include {
  path = find_in_parent_folders()
}

terraform {
  source = "git::git@bitbucket.org:arivictor/infrastructure-modules.git//project?ref=main"
}

inputs = {
  project_name      = "ari-abc-prod-1"
  project_id        = "ari-abc-prod-1"
  organisation_id   = "899654545926"
}