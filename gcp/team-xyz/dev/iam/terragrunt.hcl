include {
  path = find_in_parent_folders()
}

terraform {
  source = "git::git@bitbucket.org:arivictor/infrastructure-modules.git//project_iam_binding?ref=main"
}

dependencies {
  paths = ["../project"]
}

inputs = {
  project_id = "ari-abc-dev-1"
  role    = "roles/owner"
  members = [
      "serviceAccount:terraform@ari-admin-1.iam.gserviceaccount.com"
  ]
}