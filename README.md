# CI/CD Proof of Concept

**What does this do?**

This projects demonstrates a proof of concept for a CI/CD pipeline using the following components:

* Bitbucket for code versioning control
* Google Cloud Platform - Cloud Build for automation
* Google Cloud Platform - Secrets Manager for SSH keys
* Terraform for infrastructure as code
* Terragrunt for Terraform enhancement




**What is what?**

* `builder/` holds the `cloudbuild.yaml` and `Dockerfile` to create a custom image running Terraform and Terragrunt. This image is used later in the pipeline.
* `environments` contains subdirectories for all the environments. Each environment subdirectory will contain the projects and their respective terragrunt/terraform files.
* `./cloudbuild.yaml` is used by Cloud Build to execute the required Terragrunt (or Terraform) commands depending on the branch.

**How does it work?**

For this example Cloud Build was configured to trigger anytime code was pushed to any branch. Cloud Build would read in the `cloudbuild.yaml` file and execute the steps. The process can be enhanced by creating separate triggers for different branches.

> e.g. Only run the `apply` command when code is pushed to `main`. For all other branches just run `init` and `plan`.

The concept can be modified for Bitbucket to instead trigger a webhook in Cloud Build, this would avoid the requirement to sync repositories between GCP and Bitbucket.

> Learn how to [build hosted repos on Bitbucket Cloud](https://cloud.google.com/build/docs/automating-builds/build-hosted-repos-bitbucket-cloud)

**Prerequisites**

* Configure a project to run Cloud Build and provide the required permissions to the service account.

> The cloud build service account should be added to the projects as an Editor to be able to deploy resources.

* Cloud Storage bucket to hold the state files. Terragrunt could be configured to have separate buckets and projects for each environment or it can all be actioned from the same account.

**Alerts & Logging**

Cloud Build logs are exported to Cloud Loggings, alerts can be configured on relevant metrics to send emails or webhook triggers

* Build Success
* Build Failure
* Custom metrics/regex

**Security & Permissions**

The Cloud Build service account or the service account used by Cloud Build should be added to each projects IAM it will deploy resources to. For the purpose of this proof of concept the following roles were given to the Cloud Build service account:

* Project Creator

**Note**

* Bitbucket does not integrate to the same extent as GitHub, therefore the Bitbucket repository is synchronised to Google Cloud Source Repositories in the project Cloud Build is configured. This allows Cloud Build to watch for changes.

> Bitbucket does not support native triggers on Pull Requests. Webhooks could be setup instead, otherwise branch naming conventions could be used along with some logical conditions in the `cloudbuild.yaml` file.

**Resources**

- [Cloud Build Documentation](https://cloud.google.com/build/docs/how-to)
- [Cloud Builders Community | Terragrunt](https://github.com/GoogleCloudPlatform/cloud-builders-community/tree/master/terragrunt)
- [Google Cloud Platform | Terraform GitOps Example](https://github.com/GoogleCloudPlatform/solutions-terraform-cloudbuild-gitops/tree/e6bcec81715f52a9a9c7f547926fe4a05c102268)

This discusses why `<action>-all`/`run-all <action>` is not the best approach. Apply to affected module only.
- [CI/CD Best Practice for plan-all output? #720](https://github.com/gruntwork-io/terragrunt/issues/720)

This discusses a way to find which modules were affected
- [Monorepo build options idea #1206](https://github.com/gruntwork-io/terragrunt/issues/1206)


notes:
- Need to add SSH private key to secrets manager
- Add public key to every repo need access to
